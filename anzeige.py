import getpass
import logging
import os
import sys
import time
import tkinter as tk
from inspect import getsourcefile

import paho.mqtt.client as mqtt

# Wichtig: die Auflösung sollte nicht kleiner als 1024x768 sein

# Logging Abschnitt
# Benutzer pi schreibt nach /home/pi (darf nicht nach /var/tmp schreiben)
logfile_name = '/home/pi/raspi_lichtschranke/anzeige.log'
if 'pi' != getpass.getuser():
    # Benutzer root schreibt nach /var/tmp/lichtschranke.log (darf nicht nach /home/pi schreiben)
    logfile_name = '/var/tmp/anzeige.log'
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
                    handlers=[
                        logging.FileHandler(logfile_name),
                        logging.StreamHandler()
                    ])

logger = logging.getLogger(__name__)

# MQTT Funktionen
MQTT_SERVER_ADDRESSE = 'lichtschrankeserver.local'
BESUCHERANZAHL_DATEI = 'besucheranzahl.txt'
TOPIC_LICHTSCHRANKEEINGANG = 'lichtschrankeserver'  # Auf dem Server läuft auch die Lichtschranke für den Eingang
TOPIC_LICHTSCHRANKEAUSGANG = 'lichtschrankeausgang'

ABS_PROGRAMMFILE_DIR = os.path.dirname(os.path.abspath(getsourcefile(lambda: 0))) + os.sep
ABS_IMAGES_DIR_PATH = ABS_PROGRAMMFILE_DIR + 'images' + os.sep
ABS_BESUCHERANZAHL_FILE_PATH = ABS_PROGRAMMFILE_DIR + BESUCHERANZAHL_DATEI


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.connected_flag = True
        client.subscribe([(TOPIC_LICHTSCHRANKEEINGANG, 0), (TOPIC_LICHTSCHRANKEAUSGANG, 0)])
        logger.info('Verbunden mit Code: %s', rc)
    else:
        logger.error('Keine Verbindung. Stoppe Skript. Code: %s', rc)
        client.bad_connection_flag = True


def on_disconnect(client, userdata, rc):
    logger.warning(
        'Verbindung verloren: %s. Es wird versucht die Verbindung wiederherzustellen', rc)
    client.connected_flag = False


def on_message(client, userdata, message):
    logger.debug('Topic: %s, %s', message.topic, message.payload)
    if message.topic == TOPIC_LICHTSCHRANKEEINGANG:
        root.aktuell_in_attraktion += 1
        root.total += 1
        total_label['text'] = TOTAL_TEXT + str(root.total)
        in_attraktion_label['text'] = IN_ATTRAKTION + str(root.aktuell_in_attraktion)
        with open(ABS_BESUCHERANZAHL_FILE_PATH, 'w+') as file:
            file.write(str(root.total))
        if root.aktuell_in_attraktion >= 2:
            text_label['image'] = stop_image
            text_label['text'] = STOP_TEXT
            text_label['foreground'] = STOP_FOREGROUND_COLOR
    elif message.topic == TOPIC_LICHTSCHRANKEAUSGANG:
        if root.aktuell_in_attraktion > 0:
            root.aktuell_in_attraktion -= 1
            total_label['text'] = TOTAL_TEXT + str(root.total)
        in_attraktion_label['text'] = IN_ATTRAKTION + str(root.aktuell_in_attraktion)
        if root.aktuell_in_attraktion < 2:
            text_label['image'] = go_image
            text_label['text'] = GO_TEXT
            text_label['foreground'] = GO_FOREGROUND_COLOR


mqtt.Client.connected_flag = False
mqtt.Client.bad_connection_flag = False
anzeige_client = mqtt.Client()
anzeige_client.on_connect = on_connect
anzeige_client.on_disconnect = on_disconnect
anzeige_client.on_message = on_message
anzeige_client.loop_start()
logger.info('Versuche mit MQTT Server zu verbinden %s', MQTT_SERVER_ADDRESSE)
try:
    anzeige_client.connect(MQTT_SERVER_ADDRESSE, 1883, 60)
except:
    logger.warning('Verbindung fehlgeschlagen')
while not anzeige_client.connected_flag and not anzeige_client.bad_connection_flag:
    logger.info('Warte auf Verbindung')
    time.sleep(1)
if anzeige_client.bad_connection_flag:
    anzeige_client.loop_stop()
    sys.exit()

# Display Funktionen

IN_ATTRAKTION = 'In Attraktion: '
FONT = 'Helvetica'
BACKGROUND_COLOR = 'black'
GO_TEXT = 'Eine Person \ndarf an den Start'
GO_FOREGROUND_COLOR = 'green'
STOP_TEXT = 'Max. 2 \nPersonen'
STOP_FOREGROUND_COLOR = 'red'
TOTAL_TEXT = 'Besucher: '

root = tk.Tk()
root.attributes('-fullscreen', True)
root.configure(background=BACKGROUND_COLOR)
root.bind('<Escape>', lambda e: root.destroy())
root.aktuell_in_attraktion = 0
root.total = 0

# Datei für bisherige Besucheranzahl erstellen wenn noch nicht vorhanden
try:
    open(ABS_BESUCHERANZAHL_FILE_PATH, 'r')
except IOError:
    with open(ABS_BESUCHERANZAHL_FILE_PATH, 'w+') as f:
        f.write('0')

# Bisherige Besucherzahl einlesen
with open(ABS_BESUCHERANZAHL_FILE_PATH) as f:
    root.total = int(f.readline())

stop_image = tk.PhotoImage(file=ABS_IMAGES_DIR_PATH + 'stop.png').subsample(4, 4)
go_image = tk.PhotoImage(file=ABS_IMAGES_DIR_PATH + 'go.png').subsample(4, 4)

# Ein Label welches 'Eingang erlaubt' oder 'Eingang verboten' anzeigt
text_label = tk.Label(root, text=GO_TEXT, font=(FONT, 60, 'bold'), background=BACKGROUND_COLOR,
                      foreground=GO_FOREGROUND_COLOR,
                      justify=tk.CENTER, anchor='s', compound=tk.TOP, image=go_image)
text_label.pack(expand=True, fill=tk.BOTH)

# Ein Label welches die Anzahl an Personen in der Attraktion anzeigt
in_attraktion_label = tk.Label(root, text=IN_ATTRAKTION + str(root.aktuell_in_attraktion), font=(FONT, 30),
                               background=BACKGROUND_COLOR, foreground=STOP_FOREGROUND_COLOR, justify=tk.CENTER,
                               anchor='s')
in_attraktion_label.pack(expand=True, fill=tk.BOTH, anchor='s')

# Ein Label welches die Gesamtanzahl an Besuchern anzeigt
total_label = tk.Label(root, text=TOTAL_TEXT + str(root.total), font=(FONT, 30), background=BACKGROUND_COLOR,
                       foreground=STOP_FOREGROUND_COLOR, justify=tk.CENTER, anchor='s')
total_label.pack(anchor='s', fill=tk.BOTH)

root.mainloop()
