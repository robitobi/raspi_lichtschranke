# Raspi Lichtschranke

Mit Hilfe von zwei Lichtschranken und eines Displays wird sichergestellt 
dass maximal zwei Personen eine Attraktion benutzen.

Es sind zwei Raspi's im Einsatz:
* **lichtschrankeserver**: (erreichbar mittels `lichtschrankeserver.local`) 
    * dort läuft das Anzeigeprogramm welches auf Lichtschranken-Events 
    auf den MQTT Topics `lichtschrankeserver` und `lichtschrankeausgang` reagiert
    und das angeschlossene Display entsprechend steuert.
    * dort läuft der MQTT Broker welcher die beiden Topics zur Verfügung stellt
    * weiter ist eine Lichtschranke angeschlossen und `lichtschranke.py` (als Service `lichtschranke`) 
    publiziert Unterbrechungen der Lichtschranke auf dem Topic `lichtschrankeeingang`
* **lichtschrankeausgang**: (erreichbar mittels `lichtschrankeausgang.local`) 
    * es ist eine Lichtschranke angeschlossen und `lichtschranke.py` (als Service `lichtschranke`) 
    publiziert Unterbrechungen der Lichtschranke auf dem Topic `lichtschrankeausgang`

# Installation der Anwendung
#### Einschalten: 
* bitte den Raspi-Server _lichtschrankeserver_ zuerst starten (_lichtschrankeausgang_ benötigt einen laufenden MQTT Server).
#### Netzwerkverbindung:
* Netzwerkkabel anschließen oder WLAN konfigurieren
* Testen ob per _lichtschrankeserver.local_ und _lichtschrankeausgang.local_ erreichbar (_ssh_, _vnc_)
#### Lichtschranke aufbauen
* Lichtschranke mit Strom versorgen
* Prüfen ob die Lampe der Lichtschranke leuchtet (schwaches rotes Licht)
* Prüfen ob die Lichtschranke (oben) gelb blinkt wenn der Reflektor davor gehalten wird
* Prüfen ob die Lichtschranke (oben) nicht mehr blinkt wenn sie unterbrochen wird
* die Lichtschranke und den Reflektor so anbringen dass die Lichtschranke gelb blinkt
und der der rote Punkt des Lichts ungefähr in der Mitte des Reflektors zu sehen ist 
(dabei an der Lichtschranke aus auf den Reflektor schauen). Maximal drei Meter Abstand.
#### Testen ob die Anwendung korrekt arbeitet
* die Anzeige beobachten und die Lichtschranken entsprechend unterbrechen
* die Log-Datei z.B. mit `tail -f /var/tmp/lichtschranke.log` beobachten. Dort werden Unterbrechungen angezeigt.



# FAQ
1. Wie beende ich das Anzeigeprogramm auf dem Server?
    * die Taste `Escape` drücken
    * mit `kill`: 
        * PID des Python-Prozess herausfinden: 
            * `ps -A | grep python`
            * `ps -fp _PID_` liefert den Namen des aufgerufenen Python-Skripts (`anzeige.py`) 
        * `kill _PID_`
2. Wie starte ich das Anzeigeprogramm auf dem Server?
    * das Programm wird automatisch gestartet. 
    Wenn es gestoppt wurde (siehe oben) kann es durch `/usr/bin/python3 /home/pi/raspi_lichtschranke/anzeige.py` gestartet werden.
3. Wie beende ich den Lichtschrankenservice?
    * `sudo service lichtschranke stop`
4. Wie starte ich den Lichtschrankenservice?
    * der Service wird automatisch beim hochfahren gestartet
    * wenn er beendet wurde mit `sudo service lichtschranke start` starten
5. Wo finde ich die Ausgabe der Anzeige?
    * in `/home/pi/raspi_lichtschranke/anzeige.log`
6. Wo finde ich die Ausgabe der Lichtschranke?
    * in `/var/tmp/lichtschranke.log`
7. Wie kann ich die Besucheranzahl ändern?
    * die Zahl in der Datei `/home/pi/raspi_lichtschranke/besucheranzahl.txt` ändern und den Raspi neu starten            
8. Es werden zu viele Personen in der Attraktion angezeigt. Was kann ich tun?
    * die Lichtschranke am Ausgang auslösen
    * eine MQTT-Nachricht auf dem Topic `lichtschrankeausgang` mit Inhalt `unterbrochen` senden
        * `mosquitto_pub -h lichtschrankeserver.local -t lichtschrankeausgang -m "unterbrochen"`
9. Speicherkarte voll (Logging)
    * der Log-Level ist auf Debug eingestellt und dadurch wächst die Logdatei in `var/tmp/lichtschranke.log`. 
    Bei einer Fehlfunktion der Lichtschranke kann die Loddatei sehr schnell anwachsen.
    In dem Fall oder wenn die SD Karte voll ist die Log-Datei löschen und den Service neu starten `sudo service lichtschranke restart`   
10. Wie kann ich die Anwendung aktualisieren?
    * im Verzeichnis _/home/pi_raspi_lichtschranke_: `git pull`