# Konfiguration

## Zugriff auf den Raspi
* per Monitor, Tastatur und Maus
* per Netzwerk (_ssh_, _vnc_ vorher auf dem Raspi aktivieren):
    * über IP Adresse falls bekannt
    * über _Zeroconf_: 
        * Zugriff via Hostnamen
            * _raspberry_ (initial) 
            * _lichtschrankeserver.local_ und 
            * _lichtschrankeausgang.local_
        * unter Windows muss dazu [bonjour](https://support.apple.com/kb/DL999?locale=de_DE) installiert sein.
        
### Konfiguration _lichtschrankeserver_
* System
    * Hostname ändern: _lichtschrankeserver_ z.B. mit _sudo raspi-config_ -> _Network Options_ -> _Hostname_
    * Passwort ändern mit _passwd_
    * Aktiviere ssh und VNC
    * Aktiviere und konfiguriere Netzwerk z.B. WIFI
    * Setze deutsch als Sprache 
* MQTT:
    * MQTT Server und Client installieren: _sudo apt install -y mosquitto mosquitto-clients_
    * Python MQTT Client installieren: _sudo pip3 install paho-mqtt_
* Anzeigeprogramm _anzeige.py_ bei Start ausführen (GUI braucht X):
    * via _~/.config/autostart/.desktop_, siehe _dot_config_autostart_dot_desktop_
    * Logging in _/var/tmp/anzeige.log_
* Lichtschrankenprogramm _lichtschranke.py_ bei Start ausführen
    * mittels _systemd_ und _lib_systemd_system_lichtschranke.service_ (siehe auch 
    [Five Ways To Run a Program On Your Raspberry Pi At Startup](https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/#systemd))
    * Logging in _/var/tmp/lichtschranke.log_
    
### Konfiguration _lichtschrankeausgang_
* System (siehe oben)
    * Hostname ändern: _lichtschrankeausgang_
* MQTT:
    * MQTT Client installieren: _sudo apt install -y mosquitto-clients_
    * Python mqtt Client installieren: _sudo pip3 install paho-mqtt_
* Lichtschrankenprogramm bei Start ausführen (siehe oben)
