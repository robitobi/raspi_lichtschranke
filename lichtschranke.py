import getpass
import logging
import socket
import sys
import time
from signal import pause

import paho.mqtt.client as mqtt
from gpiozero import Button

# Logging Abschnitt
# Benutzer pi schreibt nach /home/pi (darf nicht nach /var/tmp schreiben)
logfile_name = '/home/pi/raspi_lichtschranke/lichtschranke.log'
if 'pi' != getpass.getuser():
    # Benutzer root schreibt nach /var/tmp/lichtschranke.log (darf nicht nach /home/pi schreiben)
    logfile_name = '/var/tmp/lichtschranke.log'
logging.basicConfig(level=logging.DEBUG,
                    format="%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s",
                    handlers=[
                        logging.FileHandler(logfile_name),
                        logging.StreamHandler()
                    ])

logger = logging.getLogger(__name__)

# MQTT Abschnitt
MQTT_TOPIC = socket.gethostname()
MQTT_SERVER_ADDRESSE = 'lichtschrankeserver.local'
logger.info('Sende Nachrichten auf Topic: %s zu %s', MQTT_TOPIC, MQTT_SERVER_ADDRESSE)


def on_connect(client, userdata, flags, rc):
    if rc == 0:
        client.connected_flag = True
        client.subscribe(MQTT_TOPIC)
        logger.info('Verbunden mit Code: %s', rc)
    else:
        logger.error('Keine Verbindung. Stoppe Skript. Code: %s', rc)
        client.bad_connection_flag = True


def on_disconnect(client, userdata, rc):
    logger.warning('Verbindung verloren: %s', rc)
    client.connected_flag = False


mqtt.Client.connected_flag = False
mqtt.Client.bad_connection_flag = False
lichtschranke_client = mqtt.Client()
lichtschranke_client.on_connect = on_connect
lichtschranke_client.on_disconnect = on_disconnect
lichtschranke_client.loop_start()
logger.info('Versuche mit MQTT Server zu verbinden %s', MQTT_SERVER_ADDRESSE)
try:
    lichtschranke_client.connect(MQTT_SERVER_ADDRESSE, 1883, 60)
except:
    logger.warning('Verbindung fehlgeschlagen')
while not lichtschranke_client.connected_flag and not lichtschranke_client.bad_connection_flag:
    logger.info('Warte auf Verbindung')
    time.sleep(1)
if lichtschranke_client.bad_connection_flag:
    lichtschranke_client.loop_stop()
    sys.exit()

# Lichtschranken Funktionen
unterbrechungStartzeit = 0.0
aktivStartzeit = time.time()
aktivDauer = 0.0
counter = 0
button = Button(17)


def unterbrochen():
    global unterbrechungStartzeit, counter, aktivStartzeit, aktivDauer
    unterbrechungStartzeit = time.time()
    aktivDauer = time.time() - aktivStartzeit
    logger.debug('Dauer aktiv: %s: %s', counter, aktivDauer)
    aktivStartzeit = 0.0


def geschlossen():
    global unterbrechungStartzeit, counter, aktivStartzeit, aktivDauer
    # ignoriere kurze Unterbrechungen z.B. durch Arme
    # ignoriere kurze Zeiten z.B. durch  GPIO Bugs
    unterbrechungs_dauer = time.time() - unterbrechungStartzeit
    logger.debug('Dauer unterbrochen: %s: %s', counter, unterbrechungs_dauer)
    if unterbrechungStartzeit > 1 and unterbrechungs_dauer > 0.03 and aktivDauer > 0.25:
        counter = counter + 1
        lichtschranke_client.publish(MQTT_TOPIC, 'unterbrochen', 0)
    unterbrechungStartzeit = 0.0
    aktivStartzeit = time.time()


button.when_pressed = unterbrochen
button.when_released = geschlossen

pause()
