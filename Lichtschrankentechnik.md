# Technik der Lichtschranke
Die Lichtschranke soll einen Abstand von mindestens 1,5 Meter überbrücken.

## Selbstbau
Wir haben drei Möglichkeiten eine Lichtschranke selbst zu bauen getestet.
* Sichtbares Licht: 
    * einfache Dioden waren nicht fokusiert genug. 
    Mit einer besseren Leuchtdiode (größer, kleinerer Abstrahlwinkel, höhere Spannung)
    konnten wir ca. 0,5 Meter erreichen.
    * Ergebnis:
        * nur über kurze Entfernung
        * braucht zwei Stromquellen: Licht, Empfänger
* Infrarotlicht
    * dazu haben wir 153870 und 185809 ausprobiert
    * Ergebnis: 
        * nur über kurze Entfernung
        * braucht zwei Stromquellen: Licht, Empfänger
* Laserdioden
    * Ergebnis
        * funktionieren über größere Entfernungen
        * Sicherheitsrisiko wenn zu starke Laser verwendet werden
        * braucht zwei Stromquellen: Licht, Empfänger

## [SICK WL12-P430](https://www.sick.com/de/de/wl14-p430/p/p4462)
Wir haben eine SICK Reflex-Lichtschranke gebraucht erworben.
Diese funktioniert sehr zuverlässig über einen Abstand bis zu 3 Metern und 
benötigt nur eine Stromquelle (24 Volt).

### Ausgänge der Lichtschranke
* _braun_: L+, 24 Volt, erster gegen den Uhrzeigersinn
* _weiß_: das Signal, zweiter gegen den Uhrzeigersinn. 
    * Hier liegt eine Spannung von 14 Volt (geschlossen) und 7 Volt (unterbrochen) an
* _blau_: M, Masse, dritter gegen den Uhrzeigersinn
* _schwarz_: Q, Signal, nicht genutzt

### Reduzierung der Spannung
Die GPIO Pins des Raspi können nur eine Spannung zwischen 0 und 3,3 Volt verarbeiten.
Der Digitale Eingang liefert kein Strom (0) bei Spannung unter 1,16 Volt  und Strom (1) für Spannung über 1,25 Volt.
Spannungen dazwischen können einen undefinierten Zustand erzeugen (ständig zwischen 0 und 1 wechseln) und der Bereich sollte nur sehr kurze Zeit anliegen.

Um die Spannung zu reduzieren haben wir einen _Spannungsteiler_ im Verhältnis 8:1 (10.000 Ohm zu 1.220 Ohm) eingebaut.
Dadurch erreicht den GPIO Pin 17 dann eine Spannung von ~0.7 (unterbrochen) und ~1.5 Volt (geschlossen).
Ein Test mit einem günstigen Spannungswandler (LM2596S) war nicht erfolgreich da das Modul nicht so schnell schaltet.

###  Gemessene Zeiten
Wir benutzen _gpiozero_ weil das damit wesentlich einfacher ist auf GPIO Signale zu reagieren und die Logik umzusetzen.
Zeiten: 
* schneller Durchgang (rennen): ca. 50ms
* normaler Durchgang (gehen): ca. 250ms
* Armschlenker beim Gehen: bis zu 200 ms
* Abstand wenn zwei Personen nahe hintereinander durchgehen: ca. 250ms